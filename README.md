#T120B516 Objektinis programų projektavimas


##Importing
```
To import in Eclipse: File -> Import -> Gradle -> Gradle Project
To import to Intellij IDEA: File -> Open -> build.gradle
To import to NetBeans: File -> Open Project...
```

##Setting up launching settings
```
First launch: 
%PROJECT_DIR%/desktop/src/edu.ktu.t120b516.rpg.desktop/DesktopLauncher.java

Then:
Change working directory to: %PROJECT_DIR%/core/assets

Should work afterwards
```