package edu.ktu.t120b516.rpg;

public class Resource implements IRealItem {
    private String name;
    private int image;
    private String description;

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public IRealItem makeCopy() {

        Resource itemObject = null;

        try {
            itemObject = (Resource) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return itemObject;
    }

    @Override
    public void accept(IStat stats) {

    }

    @Override
    public boolean isNil() {
        return false;
    }
}
