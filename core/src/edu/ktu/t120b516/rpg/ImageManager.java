package edu.ktu.t120b516.rpg;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class ImageManager {
	
	private Texture[] images = new Texture[4];
	
	public void loadImages() {
		images[0] = new Texture(Gdx.files.internal("images/doge.png"));
		images[1] = new Texture(Gdx.files.internal("images/grass.png"));
		images[2] = new Texture(Gdx.files.internal("images/sand.png"));
		images[3] = new Texture(Gdx.files.internal("images/stone.png"));
		System.out.println("Loading images using ImageManager");
	}
	
	public Texture getImage(int id) {
		return images[id];
	}
	
}
