package edu.ktu.t120b516.rpg;

public class ResourceEngineer {
    private IResourceBuilder resourceBuilder;

    public ResourceEngineer(IResourceBuilder resourceBuilder) {
        this.resourceBuilder = resourceBuilder;
    }

    public ResourceEntity getResource() {
        return  this.resourceBuilder.getResource();
    }

    public void makeResource() {
        this.resourceBuilder.buildName();
        this.resourceBuilder.buildImage();
        this.resourceBuilder.buildMinSkillLevel();
        this.resourceBuilder.buildSkillRequired();
        this.resourceBuilder.buildAvailableItemDrops();
    }
}
