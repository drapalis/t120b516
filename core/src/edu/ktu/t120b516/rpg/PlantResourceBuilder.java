package edu.ktu.t120b516.rpg;

public class PlantResourceBuilder implements IResourceBuilder {

    private ResourceEntity resourceEntity;

    PlantResourceBuilder() {
        this.resourceEntity = new ResourceEntity();
    }

    @Override
    public void buildName() {
        this.resourceEntity.setName("Stick");
    }

    @Override
    public void buildImage() {
        this.resourceEntity.setImage(0); //TODO: set actual image from sprite manager
    }

    @Override
    public void buildMinSkillLevel() {
        this.resourceEntity.setMinSkillLevel(1);
    }

    @Override
    public void buildSkillRequired() {
        this.resourceEntity.setSkillRequired(0); //TODO: set actual skill id when skill enumerator/manager is made
    }

    @Override
    public void buildAvailableItemDrops() {

        Resource grass = new Resource();
        grass.setName("Grass");
        grass.setImage(0); // TODO: set actual image for pebble
        grass.setDescription("Grass, mostly used for clothing");
        Resource poppy = new Resource();
        poppy.setName("Poppy");
        poppy.setImage(0); // TODO: set actual image for pebble
        poppy.setDescription("Poppy, just don't get high!");

        //TODO: should make item / resource manager with all available resources pre-defined

        Resource drops[] = new Resource[] {grass, poppy};
        this.resourceEntity.setAvailableDrops(drops);
    }

    @Override
    public ResourceEntity getResource() {
        return this.resourceEntity;
    }
}
