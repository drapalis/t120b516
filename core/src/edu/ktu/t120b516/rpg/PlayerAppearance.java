package edu.ktu.t120b516.rpg;

public class PlayerAppearance {

    private String gender;
    private String hair;
    private String body;
    private String shirt;
    private String pants;
    private String shoes;

    PlayerAppearance(String gender, String hair, String body, String shirt, String pants, String shoes) {
        this.gender = gender;
        this.hair = hair;
        this.body = body;
        this.shirt = shirt;
        this.pants = pants;
        this.shoes = shoes;
    }

    PlayerAppearance() {}

    public PlayerAppearanceMemento saveState() {
        return new PlayerAppearanceMemento(this.gender, this.hair, this.body, this.shirt, this.pants, this.shoes);
    }

    public PlayerAppearanceMemento setGender(String gender) {
        this.gender = gender;
        return this.saveState();
    }

    public PlayerAppearanceMemento setHair(String hair) {
        this.hair = hair;
        return this.saveState();
    }

    public PlayerAppearanceMemento setBody(String body) {
        this.body = body;
        return this.saveState();
    }

    public PlayerAppearanceMemento setShirt(String shirt) {
        this.shirt = shirt;
        return this.saveState();
    }

    public PlayerAppearanceMemento setPants(String pants) {
        this.pants = pants;
        return this.saveState();
    }

    public PlayerAppearanceMemento setShoes(String shoes) {
        this.shoes = shoes;
        return this.saveState();
    }

    public String getGender() {
        return gender;
    }

    public String getHair() {
        return hair;
    }

    public String getBody() {
        return body;
    }

    public String getShirt() {
        return shirt;
    }

    public String getPants() {
        return pants;
    }

    public String getShoes() {
        return shoes;
    }

    public void undo(PlayerAppearanceMemento playerAppearanceMemento) {
        this.gender = playerAppearanceMemento.getGender();
        this.body = playerAppearanceMemento.getBody();
        this.hair = playerAppearanceMemento.getHair();
        this.shirt = playerAppearanceMemento.getShirt();
        this.pants = playerAppearanceMemento.getPants();
        this.shoes = playerAppearanceMemento.getShoes();
    }
}
