package edu.ktu.t120b516.rpg;

public abstract class RandomPickupTemplate {

    private int positionX, positionY, quantity;
    private IRealItem item;

    public final int getPositionX() {
        return positionX;
    }

    public final int getPositionY() {
        return positionY;
    }

    public final int getQuantity() {
        return quantity;
    }

    public final IRealItem getItem() {
        return item;
    }

    public final void makePickup() {
        position();
        quantity();
        item();
    }

    public void position() {
        this.positionX = setPositionX();
        this.positionY = setPositionY();
    }

    public int setPositionX() {
        return (int)(Math.random()*1000);
    }

    public int setPositionY() {
        return (int)(Math.random()*1000);
    }

    public void quantity() {
        this.quantity = setQuantity();
    }

    public int setQuantity() {
        return 1;
    }

    public void item() {
        this.item = setItem();
    }

    public abstract IRealItem setItem();


}
