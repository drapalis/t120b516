package edu.ktu.t120b516.rpg;
public class ResourceEntity implements IResourceEntity {

    private String name;
    private int image;
    private int minSkillLevel;
    private int skillRequired;
    private Resource availableItemDrops[];

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public void setMinSkillLevel(int level) {
        this.minSkillLevel = level;
    }

    @Override
    public void setSkillRequired(int skill) {
        this.skillRequired = skill;
    }

    @Override
    public void setAvailableDrops(Resource items[]) {
        this.availableItemDrops = items;
    }

    public String getName() {
        return this.name;
    }

    public int getImage() {
        return this.image;
    }

    public int getMinSkillLevel() {
        return this.minSkillLevel;
    }

    public int getSkillRequired() {
        return this.skillRequired;
    }

    public Resource[] getAvailableItemDrops() {
        return availableItemDrops;
    }
}
