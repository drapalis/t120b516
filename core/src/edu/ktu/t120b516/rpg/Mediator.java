package edu.ktu.t120b516.rpg;

public interface Mediator {
	public void callForAssistance(Vector2 position, Enemy enemy);
	public void addEnemy(Enemy enemy);
}
