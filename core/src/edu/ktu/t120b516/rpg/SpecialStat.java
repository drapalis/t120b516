package edu.ktu.t120b516.rpg;

import java.util.Random;

public class SpecialStat implements IStat {

    private PlayerData player;

    SpecialStat(PlayerData player){ this.player = player; }

    @Override
    public void visit(Weapon weapon) {
        int playerLevel = player.getLevel();
        int maxDamage = weapon.getDamageMax();
        int minDamage = weapon.getDamageMin();

        Random randDmg = new Random();
        int randomDmgValue = randDmg.nextInt((maxDamage - minDamage) + 1) + minDamage;

        int damage = (3 + randomDmgValue + playerLevel * 3)*2;

        System.out.println("Calculated special damage: " + damage);
    }

    @Override
    public void visit(Armor armor) {
        int playerLevel = player.getLevel();
        int armorDefence = armor.getDefence();

        int defence = ( + 10 + armorDefence + playerLevel * 7)*2;

        System.out.println("Calculated special defence: " + defence);
    }
}