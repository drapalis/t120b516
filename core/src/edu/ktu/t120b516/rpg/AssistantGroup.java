package edu.ktu.t120b516.rpg;
import java.util.ArrayList;
import java.util.Iterator;

public class AssistantGroup implements Mediator{

	ArrayList enemies = new ArrayList();
	
	@Override
	public void callForAssistance(Vector2 position, Enemy enemy) {
		Iterator enemyIterator = enemies.iterator();
		
		while(enemyIterator.hasNext()) {
			Enemy temp = (Enemy) enemyIterator.next();
			if(distance(position, temp.getPosition()) <= 10) {
				if(temp != enemy) {
					temp.receiveAssistanceRequest(position);
				}
			}
		}
	}

	@Override
	public void addEnemy(Enemy enemy) {
		enemies.add(enemy);
	}
	
	private double distance(Vector2 pos1, Vector2 pos2) {
		return Math.sqrt(((pos2.getX()-pos1.getX())*(pos2.getX()-pos1.getX()))+((pos2.getY()-pos1.getY())*(pos2.getY()-pos1.getY())));
	}

}
