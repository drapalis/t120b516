package edu.ktu.t120b516.rpg;

public class EnemySnake extends Enemy{
    public EnemySnake(double hp, double dmg)
    {
        setName("Snake");
        setHitPoints(hp);
        setDealsDamage(dmg);
    }

    public void bite()
    {

    }
}
