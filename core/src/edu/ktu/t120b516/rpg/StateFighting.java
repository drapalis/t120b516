package edu.ktu.t120b516.rpg;

public class StateFighting implements IPlayerState {

    @Override
    public void doAction(PlayerData player) {
        player.setState(this);
    }

    @Override
    public String toString(){
        return "Player is fighting";
    }
}