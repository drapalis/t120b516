package edu.ktu.t120b516.rpg;

public class OreResourceBuilder implements IResourceBuilder {

    private ResourceEntity resourceEntity;

    OreResourceBuilder() {
        this.resourceEntity = new ResourceEntity();
    }

    @Override
    public void buildName() {
        this.resourceEntity.setName("Rock");
    }

    @Override
    public void buildImage() {
        this.resourceEntity.setImage(0); //TODO: set actual image from sprite manager
    }

    @Override
    public void buildMinSkillLevel() {
        this.resourceEntity.setMinSkillLevel(1);
    }

    @Override
    public void buildSkillRequired() {
        this.resourceEntity.setSkillRequired(0); //TODO: set actual skill id when skill enumerator/manager is made
    }

    @Override
    public void buildAvailableItemDrops() {

        Resource pebble = new Resource();
        pebble.setName("Pebble");
        pebble.setImage(0); // TODO: set actual image for pebble
        pebble.setDescription("Pebble, good for nothing resource");
        Resource gravel = new Resource();
        gravel.setName("Gravel");
        gravel.setImage(0); // TODO: set actual image for pebble
        gravel.setDescription("Gravel, get billion time more of those and You'll be able to make a gravel road");

        //TODO: should make item / resource manager with all available resources pre-defined

        Resource drops[] = new Resource[] {pebble, gravel};
        this.resourceEntity.setAvailableDrops(drops);
    }

    @Override
    public ResourceEntity getResource() {
        return this.resourceEntity;
    }
}
