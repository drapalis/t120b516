package edu.ktu.t120b516.rpg;
public interface IResourceEntity {
    void setName(String name);
    void setImage(int image);
    void setMinSkillLevel(int level);
    void setSkillRequired(int skill);
    void setAvailableDrops(Resource items[]);
}
