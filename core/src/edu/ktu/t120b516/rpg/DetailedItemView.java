package edu.ktu.t120b516.rpg;

public class DetailedItemView extends ItemView {
    DetailedItemView(IItem item) {
        super(item);
    }

    @Override
    public void show() {
        System.out.println("Name: " + item.getName() + "\nImage: " + item.getImage() + "\nDescription: " + item.getDescription());
    }
}
