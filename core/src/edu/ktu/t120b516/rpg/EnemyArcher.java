package edu.ktu.t120b516.rpg;

public class EnemyArcher extends Enemy{
    public EnemyArcher(double hp, double dmg)
    {
        setName("Archer");
        setHitPoints(hp);
        setDealsDamage(dmg);
    }
}
