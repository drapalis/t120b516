package edu.ktu.t120b516.rpg;

public class ArmorItem implements IItem {

    private Armor armor;

    ArmorItem(Armor armor) {
        this.armor = armor;
    }

    @Override
    public String getName() {
        return this.armor.getName();
    }

    @Override
    public int getImage() {
        return this.armor.getImage();
    }

    @Override
    public String getDescription() {
        return this.armor.getDescription() + "\nArmor effect: " + this.armor.getEffectName() + " (" + this.armor.getEffectPower() + ")\n"
                + "Armor defence: " + this.armor.getDefence();
    }
}
