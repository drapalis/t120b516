package edu.ktu.t120b516.rpg;

public class WarningMessenger extends Messenger{
	
	public WarningMessenger(int level) {
		this.level = level;
	}
	
	@Override
	protected void write(String message) {
		System.out.println("Warning message: "+message);
	}
	
}