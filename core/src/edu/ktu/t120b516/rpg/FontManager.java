package edu.ktu.t120b516.rpg;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class FontManager {
	
	private FreeTypeFontGenerator[] generator = new FreeTypeFontGenerator[1];
	
	public void loadFonts() {
		generator[0] = new FreeTypeFontGenerator(Gdx.files.internal("fonts/myFont.ttf"));
		System.out.println("Loading fonts using FontManager");
	}
	
	public BitmapFont getFont(int id, int size, Color color) {
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		parameter.color = color;
		BitmapFont font = generator[id].generateFont(parameter);
		return font;
	}
}
