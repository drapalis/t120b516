package edu.ktu.t120b516.rpg;
import com.badlogic.gdx.graphics.Texture;
import java.util.HashMap;

class MapFlyweight {

    private static final HashMap<Texture, MapTile> mapTiles = new HashMap<Texture, MapTile>();

    public static MapTile getTile(Texture texture) {
        MapTile mapTile = mapTiles.get(texture);

        if(mapTile == null) {
            mapTile = new MapTile(texture);
            mapTiles.put(texture, mapTile);
        }

        return mapTile;
    }

}

class MapTile {
    private Texture texture;

    MapTile(Texture texture) {
        this.texture = texture;
    }

    public Texture getTexture() {
        return texture;
    }
}