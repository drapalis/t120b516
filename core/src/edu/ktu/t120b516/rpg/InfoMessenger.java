package edu.ktu.t120b516.rpg;

public class InfoMessenger extends Messenger{

	public InfoMessenger(int level) {
		this.level = level;
	}
	
	@Override
	protected void write(String message) {
		System.out.println("Informational message: "+message);
	}

}
