package edu.ktu.t120b516.rpg;
public interface IResourceBuilder {
    void buildName();
    void buildImage();
    void buildMinSkillLevel();
    void buildSkillRequired();
    void buildAvailableItemDrops();
    ResourceEntity getResource();
}
