package edu.ktu.t120b516.rpg;

public class RandomCavePickup extends RandomPickupTemplate {
    @Override
    public int setPositionX() {
        return (int)(Math.random()*500);
    }

    @Override
    public int setPositionY() {
        return (int)(Math.random()*500)+500;
    }

    @Override
    public int setQuantity() {
        return 64;
    }

    @Override
    public IRealItem setItem() {
        Resource resource = new Resource();
        resource.setName("Stone Block");
        resource.setDescription("This is just a stone block...");
        resource.setImage(103);
        return resource;
    }
}
