package edu.ktu.t120b516.rpg;

public class RabbitAdapter {
    private Rabbit rabbit;
    RabbitAdapter() {
        rabbit = new Rabbit();
    }
    RabbitAdapter(Rabbit rabbit) {
        this.rabbit = rabbit;
    }
    void walk() {
        rabbit.hop();
    }

    Vector2 getPosition() {
        return rabbit.getPosition();
    }
}
