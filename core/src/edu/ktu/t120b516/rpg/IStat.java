package edu.ktu.t120b516.rpg;

public interface IStat {
    public void visit(Weapon weapon);
    public void visit(Armor armor);
}
