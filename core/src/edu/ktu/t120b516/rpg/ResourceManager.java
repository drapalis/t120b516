package edu.ktu.t120b516.rpg;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class ResourceManager {
	private ImageManager image;
	private FontManager font;
	private SoundManager sound;
	
	public ResourceManager() {
		image = new ImageManager();
		font = new FontManager();
		sound = new SoundManager();
	}
	
	public void load() {
		image.loadImages();
		font.loadFonts();
		sound.loadSounds();
	}
	
	public Object get(int id){
		if((int)(id/100) == 1)
			return image.getImage(id%100);
		if((int)(id/100) == 2)
			return sound.getSound(id%100);
		else return null;
	}
	
	public Object get(int id, int size, Color color) {
		if((int)(id/100) == 3)
			return font.getFont(id%100, size, color);
		else return null;
	}
}
