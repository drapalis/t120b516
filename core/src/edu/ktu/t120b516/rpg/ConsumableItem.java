package edu.ktu.t120b516.rpg;

public class ConsumableItem implements IItem {

    private Consumable consumable;

    ConsumableItem(Consumable consumable) {
        this.consumable = consumable;
    }

    @Override
    public String getName() {
        return this.consumable.getName();
    }

    @Override
    public int getImage() {
        return this.consumable.getImage();
    }

    @Override
    public String getDescription() {
        return this.consumable.getDescription() + "\nArmor effect: " + this.consumable.getEffectName() + " (" + this.consumable.getEffectPower() + ")";
    }
}
