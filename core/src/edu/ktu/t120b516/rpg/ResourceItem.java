package edu.ktu.t120b516.rpg;

public class ResourceItem implements IItem {

    private Resource resource;

    ResourceItem(Resource resource) {
        this.resource = resource;
    }

    @Override
    public String getName() {
        return this.resource.getName();
    }

    @Override
    public int getImage() {
        return this.resource.getImage();
    }

    @Override
    public String getDescription() {
        return this.resource.getDescription();
    }
}
