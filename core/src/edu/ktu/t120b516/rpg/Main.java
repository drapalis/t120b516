package edu.ktu.t120b516.rpg;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Main extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	ResourceManager resources;

	private void builder() {
        System.out.println("\n\nBUILDER (Darius Rapalis)");
        ResourceEngineer resourceEngineer = new ResourceEngineer(new OreResourceBuilder());
        resourceEngineer.makeResource();

        ResourceEntity resourceEntity = resourceEngineer.getResource();

        System.out.println("Resource Build");
        System.out.println("Name: " + resourceEntity.getName());
        System.out.println("Image: " + resourceEntity.getImage());
        System.out.println("Min Skill Level: " + resourceEntity.getMinSkillLevel());
        System.out.println("Skill Required: " + resourceEntity.getSkillRequired());
        System.out.print("Item drops:");
        for(int i = 0; i < resourceEntity.getAvailableItemDrops().length; i++) {
            System.out.print(" '" + resourceEntity.getAvailableItemDrops()[i].getName() + "'");
        }
    }

    private void bridge() {
        System.out.println("\n\nBRIDGE (Darius Rapalis)");

        System.out.println("\nDetailed View");
        //DETAILED VIEW
	    Weapon weapon = new Weapon();
	    weapon.setName("Crescent Rose");
	    weapon.setImage(1);
	    weapon.setDescription("High-Caliber Sniper-Scythe which Ruby Rose designed herself at Signal Academy.");
	    weapon.setDamageMin(150);
	    weapon.setDamageMax(500);
	    weapon.setEffect(5);
	    weapon.setEffectName("Stun");
	    weapon.setEffectPower(5.8);

	    WeaponItem weaponItem = new WeaponItem(weapon);

	    DetailedItemView detailedItemView = new DetailedItemView(weaponItem);
	    detailedItemView.show();

        System.out.println("\n\nShort View");
	    //SHORT VIEW
	    Armor armor = new Armor();
	    armor.setName("Beacon Academy's uniform");
	    armor.setImage(4);
	    armor.setDescription("Uniform all students at Beacon Academy shall wear.");
	    armor.setDefence(15);
	    armor.setEffect(2);
	    armor.setEffectName("Classy");
	    armor.setEffectPower(0.5);

	    ArmorItem armorItem = new ArmorItem(armor);

	    ShortItemView shortItemView = new ShortItemView(armorItem);
	    shortItemView.show();


        System.out.println("\n\nThumbnail View");
	    //THUMBNAIL VIEW
        Consumable consumable = new Consumable();
        consumable.setName("Cookie");
        consumable.setImage(2);
        consumable.setDescription("Better hide them from Ruby Rose");
        consumable.setEffect(0);
        consumable.setEffectName("Hyperactive");
        consumable.setEffectPower(15.8);

        ConsumableItem consumableItem = new ConsumableItem(consumable);

        ThumbnailItemView thumbnailItemView = new ThumbnailItemView(consumableItem);
        thumbnailItemView.show();

    }

	private void prototype() {
		System.out.println("\n\nPROTOTYPE (Edvinas Guzulaitis)");

		Weapon weapon = new Weapon();
		weapon.setName("Originalus weapon");
		weapon.setImage(22);
		weapon.setDescription("Testavimui skirtas ginklas.");
		weapon.setDamageMin(999);
		weapon.setDamageMax(999);
		weapon.setEffect(77);
		weapon.setEffectName("Stun");
		weapon.setEffectPower(5.8);

		WeaponItem weaponItem = new WeaponItem(weapon);

		DetailedItemView detailedItemView = new DetailedItemView(weaponItem);
		detailedItemView.show();

		Weapon clonedWeapon = (Weapon) weapon.makeCopy();

		System.out.println("");

		clonedWeapon.setName("Klonuotas weapon");
		WeaponItem weaponItem2 = new WeaponItem(clonedWeapon);

		DetailedItemView detailedItemView2 = new DetailedItemView(weaponItem2);
		detailedItemView2.show();

		System.out.println("\n\n[TEST] Original hash code: " + weapon.hashCode());
		System.out.println("[TEST] Clone hash code: " + clonedWeapon.hashCode());
	}

	private void factory() {
		System.out.println("\n\nFACTORY (Edvinas Guzulaitis)");

		EnemyFactory enemyFactory = new EnemyFactory();

		Enemy enemy1 = enemyFactory.makeEnemy();
		Enemy enemy2 = enemyFactory.makeEnemy();
		Enemy enemy3 = enemyFactory.makeEnemy();
		Enemy enemy4 = enemyFactory.makeEnemy();
		Enemy enemy5 = enemyFactory.makeEnemy();

		System.out.println("\nEnemy name: " + enemy1.getName() + " (HP: " + enemy1.getHitPoints() +", Deals damage: " + enemy1.getDealsDamage() + ")");
		System.out.println("\nEnemy name: " + enemy2.getName() + " (HP: " + enemy2.getHitPoints() +", Deals damage: " + enemy2.getDealsDamage() + ")");
		System.out.println("\nEnemy name: " + enemy3.getName() + " (HP: " + enemy3.getHitPoints() +", Deals damage: " + enemy3.getDealsDamage() + ")");
		System.out.println("\nEnemy name: " + enemy4.getName() + " (HP: " + enemy4.getHitPoints() +", Deals damage: " + enemy4.getDealsDamage() + ")");
		System.out.println("\nEnemy name: " + enemy5.getName() + " (HP: " + enemy5.getHitPoints() +", Deals damage: " + enemy5.getDealsDamage() + ")");
	}

	private void strategy() {
		System.out.println("\n\nSTRATEGY PATTERN (Deividas Rapalis)\n");
		
		EnemyFactory enemyFactory = new EnemyFactory();
		Enemy enemy = enemyFactory.makeEnemy();
		
		System.out.println("\nMAIN: Calling method move()");
		enemy.move();
		System.out.println("\nMAIN: Changing movement strategy");
		enemy.setMovementStrategy(new FindStraightPathStrategy());
		System.out.println("MAIN: Calling method move()");
		enemy.move();
		System.out.println("\nMAIN: Changing movement strategy");
		enemy.setMovementStrategy(new FindShortestPathStrategy());
		System.out.println("MAIN: Calling method move()");
		enemy.move();
		
	}
	
	private void facade() {
		System.out.println("\n\nFACADE PATTERN (Deividas Rapalis)\n");
		
		resources = new ResourceManager();
		
		System.out.println("\nMAIN: Calling method load() from ResourceManager\n");

		resources.load();

		img = (Texture) resources.get(100);
	}

	private void singleton() {
		//loading player data
		System.out.println("\n\nSingleton\n");
		PlayerData playerData = Save.getInstance().getLoadedData();
		System.out.println("Level: " + playerData.getLevel());
		System.out.println("Experience: " + playerData.getExp());

		PlayerData newPlayerData = new PlayerData();
		newPlayerData.setExp(playerData.getExp() * 2 + 1);
		newPlayerData.setLevel(playerData.getLevel() + 1);
		//saving new data
		Save.getInstance().save(newPlayerData);

		//getting new player data
		playerData = Save.getInstance().getLoadedData();
		System.out.println("Level: " + playerData.getLevel());
		System.out.println("Experience: " + playerData.getExp());
	}

	private void adapter() {

		Dog dog = new Dog();
		Rabbit rabbit = new Rabbit();
		RabbitAdapter rabbitAdapter = new RabbitAdapter(rabbit);

		System.out.println("Dog position: " + dog.getPosition().getX() + ":" + dog.getPosition().getY());
		dog.walk();
		System.out.println("Dog position: " + dog.getPosition().getX() + ":" + dog.getPosition().getY());

		System.out.println("Rabbit position: " + rabbitAdapter.getPosition().getX() + ":" + rabbitAdapter.getPosition().getY());
		rabbitAdapter.walk();
		System.out.println("Rabbit position: " + rabbitAdapter.getPosition().getX() + ":" + rabbitAdapter.getPosition().getY());

	}

	private void state() {
		System.out.println("\n\nSTATE (Edvinas Guzulaitis)");
		// Creating player
		PlayerData player1 = new PlayerData();

		// Creating states
		IPlayerState standingState = new StateStanding();
		IPlayerState movingState = new StateMoving();
		IPlayerState eatingState = new StateEating();
		IPlayerState fightingState = new StateFighting();

		// Setting states
		standingState.doAction(player1);
		System.out.println(player1.getState().toString());

		movingState.doAction(player1);
		System.out.println(player1.getState().toString());

		eatingState.doAction(player1);
		System.out.println(player1.getState().toString());

		fightingState.doAction(player1);
		System.out.println(player1.getState().toString());

	}

	private void visitor() {
		System.out.println("\n\nVISITOR (Edvinas Guzulaitis)");
		// Creating player
		PlayerData player1 = new PlayerData();
		player1.setLevel(1);

		Weapon weapon = new Weapon();
		weapon.setDamageMin(11);
		weapon.setDamageMax(17);

		Armor armor = new Armor();
		armor.setDefence(22);

		IStat statNormal = new NormalStat(player1);
		IStat statSpecial = new SpecialStat(player1);

		weapon.accept(statNormal);
		armor.accept(statNormal);

		weapon.accept(statSpecial);
		armor.accept(statSpecial);

	}

	private void composite() {
		
		System.out.println("\n\nCOMPOSITE (Deividas Rapalis)");
		
		// Creating a main backpack object
		BackpackObject mainBackpack = new Backpack("Main Backpack");
		
		// Creating other backpacks
		BackpackObject backpackGreen = new Backpack("Green Backpack");
		BackpackObject backpackRed = new Backpack("Red Backpack");
		BackpackObject backpackBlue = new Backpack("Blue Backpack");
		
		// Creating items
		Armor armor = new Armor();
	    armor.setName("Golden Armor");
	    armor.setImage(4);
	    armor.setDescription("Uniform all students at Beacon Academy shall wear.");
	    armor.setDefence(15);
	    armor.setEffect(2);
	    armor.setEffectName("Classy");
	    armor.setEffectPower(0.5);
	    
	    BackpackObject itemArmor1 = new BackpackItem(armor);
	    BackpackObject itemArmor2 = new BackpackItem(armor);
	    BackpackObject itemArmor3 = new BackpackItem(armor);
	    BackpackObject itemArmor4 = new BackpackItem(armor);
	    
	    // Adding items to the backpacks
	    mainBackpack.add(itemArmor1);
	    backpackGreen.add(itemArmor2);
	    backpackRed.add(itemArmor3);
	    backpackRed.add(itemArmor4);
	    
	    // Adding backpack to the other backpacks
	    mainBackpack.add(backpackGreen);
	    mainBackpack.add(backpackRed);
	    backpackRed.add(backpackBlue);
	    
	    // printing the main backpack full content
	    mainBackpack.showContent(0);


	    // Null Object pattern testing

		System.out.println("\n\nNULL OBJECT");

		IRealItem receivedItem = mainBackpack.findInBackpack("Golden Armor");

		if(!receivedItem.isNil())
		{
			System.out.println("Item " + receivedItem.getName() + " was found in a Backpack!");
		}
		else
		{
			System.out.println("Item was not found!");
		}

		
	}
	
	private void mediator() {
		
		System.out.println("\n\nMEDIATOR (Deividas Rapalis)");
		
		// Creating a mediator
		Mediator assistantGroup = new AssistantGroup();
		
		// Creating enemies and setting mediator to them
		Enemy snake1 = new EnemySnake(100, 10);
		Enemy snake2 = new EnemySnake(100, 10);
		Enemy snake3 = new EnemySnake(100, 10);
		Enemy wizard1 = new EnemyWizard(100, 10);
		snake1.setPosition(new Vector2(1, 1));
		snake2.setPosition(new Vector2(4, 2));
		snake3.setPosition(new Vector2(15, 19));
		wizard1.setPosition(new Vector2(2, 2));
		snake1.setMediator(assistantGroup);
		snake2.setMediator(assistantGroup);
		snake3.setMediator(assistantGroup);
		wizard1.setMediator(assistantGroup);
		
		// Adding enemies to the mediator
		assistantGroup.addEnemy(snake1);
		assistantGroup.addEnemy(snake2);
		assistantGroup.addEnemy(snake3);
		assistantGroup.addEnemy(wizard1);
		
		// Sending help request
		snake1.callForAssistance();
	}

	private void memento() {

		System.out.println("\n\nMEMENTO (Darius Rapalis)");

		PlayerCustomizer playerCustomizer = new PlayerCustomizer(
				"Male",
				"Fit",
				"Long",
				"Scrap",
				"Denim",
				"Boots"
		);
		System.out.println("Player Appearance Created");
		System.out.println(playerCustomizer.showPlayerAppearance());

		playerCustomizer.setHair("Short");
		System.out.println("Player Appearance Changed");
		System.out.println(playerCustomizer.showPlayerAppearance());

		playerCustomizer.setGender("Female");
		System.out.println("Player Appearance Changed");
		System.out.println(playerCustomizer.showPlayerAppearance());

		playerCustomizer.undo();
		System.out.println("Player Appearance Undo");
		System.out.println(playerCustomizer.showPlayerAppearance());

		playerCustomizer.undo();
		System.out.println("Player Appearance Undo");
		System.out.println(playerCustomizer.showPlayerAppearance());

		playerCustomizer.undo();
		System.out.println("Player Appearance Undo");
		System.out.println(playerCustomizer.showPlayerAppearance());

		playerCustomizer.redo();
		System.out.println("Player Appearance Undo");
		System.out.println(playerCustomizer.showPlayerAppearance());

		playerCustomizer.redo();
		System.out.println("Player Appearance Undo");
		System.out.println(playerCustomizer.showPlayerAppearance());

		playerCustomizer.redo();
		System.out.println("Player Appearance Undo");
		System.out.println(playerCustomizer.showPlayerAppearance());


	}

	private void flyweight() {

		for(int x = 0; x < 16; x++) {
			for(int y = 0; y < 16; y++) {
				Texture texture = MapFlyweight.getTile((Texture)resources.get(101 + ( (int)(Math.random()*3) ))).getTexture();
				batch.draw(texture, x*16, y*16);
			}
		}

	}

	private void template() {

		System.out.println("\n\nTEMPLATE");

		RandomPickupTemplate forest, cave;
		forest = new RandomForestPickup();
		forest.makePickup();
		cave = new RandomCavePickup();
		cave.makePickup();

		ResourceItem resourceItem = new ResourceItem((Resource) forest.getItem());
		System.out.println("Image: " + resourceItem.getImage());
		System.out.println("Name: " + resourceItem.getName());
		System.out.println("Description: " + resourceItem.getDescription());
		System.out.println("Position: " + forest.getPositionX() + "x" + forest.getPositionY());
		System.out.println("Quantity: " + forest.getQuantity());

		ResourceItem resourceItem2 = new ResourceItem((Resource) cave.getItem());
		System.out.println("Image: " + resourceItem2.getImage());
		System.out.println("Name: " + resourceItem2.getName());
		System.out.println("Description: " + resourceItem2.getDescription());
		System.out.println("Position: " + cave.getPositionX() + "x" + cave.getPositionY());
		System.out.println("Quantity: " + cave.getQuantity());

	}

	// Chain Of Responsibility
	private static Messenger getChainOfMessengers() {
		// Creating new messengers and attaching the chain
		Messenger infoMessenger = new InfoMessenger(Messenger.INFO);
		Messenger warningMessenger = new WarningMessenger(Messenger.WARNING);
		Messenger dangerMessenger = new DangerMessenger(Messenger.DANGER);
		
		infoMessenger.setNextMessenger(warningMessenger);
		warningMessenger.setNextMessenger(dangerMessenger);
		
		return infoMessenger;
	}
	
	private void chainOfResponsibility() {
		
		System.out.println("\n\nCHAIN OF RESPONSIBILITY");
		
		// Creating the chain
		Messenger messenger = getChainOfMessengers();
		
		messenger.sendMessege(Messenger.INFO, "Testing messeges 1");
		messenger.sendMessege(Messenger.WARNING, "Testing messeges 2");
		messenger.sendMessege(Messenger.DANGER, "Testing messeges 3");
		
	}
	
	@Override
	public void create () {
		batch = new SpriteBatch();

		builder();
		bridge();
		prototype();
		factory();
		strategy();
		facade();
		singleton();
		adapter();
		state();
		visitor();
		composite();
		mediator();
		memento();
		template();
		chainOfResponsibility();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		if(img != null) batch.draw(img, 0, 0);

		flyweight();

		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}
}
