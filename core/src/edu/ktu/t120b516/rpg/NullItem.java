package edu.ktu.t120b516.rpg;

public class NullItem implements IRealItem {

    private String name = "";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public IRealItem makeCopy() {
        return null;
    }

    @Override
    public void accept(IStat stats) {

    }

    @Override
    public boolean isNil() {
        return true;
    }
}
