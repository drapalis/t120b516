package edu.ktu.t120b516.rpg;

public class BackpackItem extends BackpackObject{

	IRealItem item;
	
	public BackpackItem(IRealItem item) {
		this.item = item;
	}
	
	public void showContent(int level) {

		String levelSeperator = "";
		for(int i = 0; i < level; i++) {
			levelSeperator += "	";
		}

		System.out.println(levelSeperator+item);
	}

	public IRealItem findInBackpack(String itemName) {

		if(itemName.equalsIgnoreCase(item.getName())){
			return item;
		}
		return null;
	}
}
