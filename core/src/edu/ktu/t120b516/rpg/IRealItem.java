package edu.ktu.t120b516.rpg;

public interface IRealItem extends Cloneable{

    public String getName();
    public IRealItem makeCopy();
    public void accept(IStat stats);
    public boolean isNil();
}