package edu.ktu.t120b516.rpg;

public abstract class BackpackObject {

	public void add(BackpackObject backpackObject) {
		throw new UnsupportedOperationException();
	}
	
	public void remove(BackpackObject backpackObject) {
		throw new UnsupportedOperationException();
	}
	
	public BackpackObject getObject(int objectIndex) {
		throw new UnsupportedOperationException();
	}
	
	public void showContent(int level) {
		throw new UnsupportedOperationException();
	}

	public IRealItem findInBackpack(String itemName) {
		throw new UnsupportedOperationException();
	}
}
