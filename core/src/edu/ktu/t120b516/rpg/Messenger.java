package edu.ktu.t120b516.rpg;

public abstract class Messenger {

	public static int INFO = 1;
	public static int WARNING = 2;
	public static int DANGER = 3;
	
	protected int level;
	
	//next element int chain of responsibility
	protected Messenger nextMessenger;
	
	public void setNextMessenger(Messenger nextMessenger) {
		this.nextMessenger = nextMessenger;
	}
	
	public void sendMessege(int level, String message) {
		if(this.level == level) {
			write(message);
		}else {
			if(nextMessenger != null) {
				nextMessenger.sendMessege(level, message);
			}
		}
	}
	
	abstract protected void write(String message);
	
}