package edu.ktu.t120b516.rpg;

import java.util.Random;

public class NormalStat implements IStat {

    private PlayerData player;

    NormalStat(PlayerData player){ this.player = player; }

    @Override
    public void visit(Weapon weapon) {
        int playerLevel = player.getLevel();
        int maxDamage = weapon.getDamageMax();
        int minDamage = weapon.getDamageMin();

        Random randDmg = new Random();
        int randomDmgValue = randDmg.nextInt((maxDamage - minDamage) + 1) + minDamage;

        int damage = randomDmgValue + playerLevel * 3;

        System.out.println("Calculated normal damage: " + damage);
    }

    @Override
    public void visit(Armor armor) {
        int playerLevel = player.getLevel();
        int armorDefence = armor.getDefence();

        int defence = 10 + armorDefence + playerLevel * 7;

        System.out.println("Calculated normal defence: " + defence);
    }
}
