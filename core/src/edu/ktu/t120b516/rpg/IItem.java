package edu.ktu.t120b516.rpg;

public interface IItem {
    String getName();

    int getImage();

    String getDescription();
}
