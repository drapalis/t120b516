package edu.ktu.t120b516.rpg;

public class EnemyWizard extends Enemy{
    public EnemyWizard(double hp, double dmg)
    {
        setName("Wizard");
        setHitPoints(hp);
        setDealsDamage(dmg);
    }
}
