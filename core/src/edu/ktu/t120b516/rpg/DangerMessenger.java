package edu.ktu.t120b516.rpg;

public class DangerMessenger extends Messenger{

	public DangerMessenger(int level) {
		this.level = level;
	}
	
	@Override
	protected void write(String message) {
		System.out.println("Danger message: "+message);
	}
	
}
