package edu.ktu.t120b516.rpg;

public class PlayerData {

    private int level;
    private int exp;
    private IPlayerState state;

    public PlayerData() { state = null; }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public void setState(IPlayerState state){
        this.state = state;
    }

    public IPlayerState getState(){
        return state;
    }
}
