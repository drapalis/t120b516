package edu.ktu.t120b516.rpg;

public class RandomForestPickup extends RandomPickupTemplate{

    @Override
    public int setPositionX() {
        return (int)(Math.random()*500);
    }

    @Override
    public int setPositionY() {
        return (int)(Math.random()*500)+500;
    }

    @Override
    public int setQuantity() {
        return 64;
    }

    @Override
    public IRealItem setItem() {
        Resource resource = new Resource();
        resource.setName("Grass Block");
        resource.setDescription("This is just a grass block...");
        resource.setImage(101);
        return resource;
    }

}
