package edu.ktu.t120b516.rpg;

public class ShortItemView extends ItemView {

    ShortItemView(IItem item) {
        super(item);
    }

    @Override
    public void show() {
        System.out.println("Image: " + item.getImage() + "\nName: " + item.getName());
    }
}
