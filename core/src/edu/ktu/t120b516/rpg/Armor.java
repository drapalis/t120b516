package edu.ktu.t120b516.rpg;

public class Armor implements IRealItem {
    private String name;
    private int image;
    private String description;
    private int defence;
    private String effectName;
    private int effect;
    private double effectPower;

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public String getEffectName() {
        return effectName;
    }

    public void setEffectName(String effectName) {
        this.effectName = effectName;
    }

    public int getEffect() {
        return effect;
    }

    public void setEffect(int effect) {
        this.effect = effect;
    }

    public double getEffectPower() {
        return effectPower;
    }

    public void setEffectPower(double effectPower) {
        this.effectPower = effectPower;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public IRealItem makeCopy() {

        Armor itemObject = null;

        try {
            itemObject = (Armor) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return itemObject;
    }

    @Override
    public void accept(IStat stats) {
        stats.visit(this);
    }

    @Override
    public boolean isNil() {
        return false;
    }
}
