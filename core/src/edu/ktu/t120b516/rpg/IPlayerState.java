package edu.ktu.t120b516.rpg;

public interface IPlayerState {
    public void doAction(PlayerData player);
    public String toString();
}
