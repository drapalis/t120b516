package edu.ktu.t120b516.rpg;

public class StateMoving implements IPlayerState {

    @Override
    public void doAction(PlayerData player) {
        player.setState(this);
    }

    @Override
    public String toString(){
        return "Player is moving";
    }
}
