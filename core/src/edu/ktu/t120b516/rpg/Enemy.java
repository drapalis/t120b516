package edu.ktu.t120b516.rpg;

public abstract class Enemy {
	private String name;
	private double hitPoints;
	private double dealsDamage;
	private MovementStrategy moveStrategy = new FindShortestPathStrategy();
	private Vector2 current;
	private Vector2 target;
	private Mediator mediator;
	
	public void setMediator(Mediator mediator) {
		this.mediator = mediator;
	}
	
	public void callForAssistance() {
		mediator.callForAssistance(getPosition(), this);
	}
	
	public void receiveAssistanceRequest(Vector2 position) {
		System.out.println("Enemy: "+name+" is going for assistance in coordinates x: "+position.getX()+"; y: "+position.getY());
	}
	
	public void setName(String nam) {
		name = nam;
	}

	public void setHitPoints(double hp) {
		hitPoints = hp;
	}

	public void setDealsDamage(double dmg) {
		dealsDamage = dmg;
	}

	public void move() {
		current = moveStrategy.findPath(current, target);
	}
	
	public void setMovementStrategy(MovementStrategy moveStrategy) {
		this.moveStrategy = moveStrategy;
	}
	
	public void setTargetPosition(Vector2 pos) {
		target = pos;
	}

	public String getName() {
		return name;
	}

	public double getHitPoints() {
		return hitPoints;
	}

	public double getDealsDamage() {
		return dealsDamage;
	}
	
	public Vector2 getPosition() {
		return current;
	}
	
	public void setPosition(Vector2 target) {
		current = target;
	}
}
