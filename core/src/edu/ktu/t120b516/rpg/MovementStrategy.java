package edu.ktu.t120b516.rpg;

public interface MovementStrategy {
	public Vector2 findPath(Vector2 current, Vector2 target);
}
