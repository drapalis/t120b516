package edu.ktu.t120b516.rpg;

import java.util.ArrayList;

public class PlayerCustomizer {

    private ArrayList<PlayerAppearanceMemento> appearanceMementos;
    private PlayerAppearance playerAppearance;
    private int index;

    PlayerCustomizer() {
        appearanceMementos = new ArrayList<PlayerAppearanceMemento>();
        playerAppearance = new PlayerAppearance();
    }

    PlayerCustomizer(String gender, String hair, String body, String shirt, String pants, String shoes) {
        appearanceMementos = new ArrayList<PlayerAppearanceMemento>();
        playerAppearance = new PlayerAppearance(gender, hair, body, shirt, pants, shoes);
        appearanceMementos.add(playerAppearance.saveState());
        index = appearanceMementos.size() - 1;
    }

    public void setGender(String gender) {
        appearanceMementos.add(playerAppearance.setGender(gender));
        index = appearanceMementos.size() - 1;
    }

    public void setBody(String body) {
        appearanceMementos.add(playerAppearance.setBody(body));
        index = appearanceMementos.size() - 1;
    }

    public void setHair(String hair) {
        appearanceMementos.add(playerAppearance.setHair(hair));
        index = appearanceMementos.size() - 1;
    }

    public void setShirt(String shirt) {
        appearanceMementos.add(playerAppearance.setShirt(shirt));
        index = appearanceMementos.size() - 1;
    }

    public void setPants(String pants) {
        appearanceMementos.add(playerAppearance.setPants(pants));
        index = appearanceMementos.size() - 1;
    }

    public void setShoes(String shoes) {
        appearanceMementos.add(playerAppearance.setShoes(shoes));
        index = appearanceMementos.size() - 1;
    }

    public void undo() {
        index--;
        if(index < 0) index = 0;
        playerAppearance.undo(appearanceMementos.get(index));
    }

    public void redo() {
        index++;
        if(index > appearanceMementos.size()-1) index = appearanceMementos.size()-1;
        playerAppearance.undo(appearanceMementos.get(index));
    }

    public String showPlayerAppearance() {
        String appearance = "";
        appearance += "Gender: " + playerAppearance.getGender() + "\n";
        appearance += "Body: " + playerAppearance.getBody() + "\n";
        appearance += "Hair: " + playerAppearance.getHair() + "\n";
        appearance += "Shirt: " + playerAppearance.getShirt() + "\n";
        appearance += "Pants: " + playerAppearance.getPants() + "\n";
        appearance += "Shoes: " + playerAppearance.getShoes();
        return appearance;
    }

}
