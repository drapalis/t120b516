package edu.ktu.t120b516.rpg;

import java.util.Random;

public class EnemyFactory {

    public Enemy makeEnemy(){

        Enemy newEnemy = null;

        Random generator = new Random();
        int type = generator.nextInt(3);
        if(type == 1){
            return new EnemySnake(12.0, 1.2);
        }else if(type == 2) {
            return new EnemyWizard(28.0, 3.6);
        } else {
            return new EnemyArcher(45.0, 4.5);
        }
    }
}
