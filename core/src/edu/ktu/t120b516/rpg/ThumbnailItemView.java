package edu.ktu.t120b516.rpg;

public class ThumbnailItemView extends ItemView {
    ThumbnailItemView(IItem item) {
        super(item);
    }

    @Override
    public void show() {
        System.out.println("Image: " + item.getImage());
    }
}
