package edu.ktu.t120b516.rpg;

public class WeaponItem implements IItem {

    private Weapon weapon;

    WeaponItem(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return this.weapon.getName();
    }

    @Override
    public int getImage() {
        return this.weapon.getImage();
    }

    @Override
    public String getDescription() {
        return this.weapon.getDescription() + "\nWeapon effect: " + this.weapon.getEffectName() + " (" + this.weapon.getEffectPower() + ")\n"
                + "Weapon power: " + this.weapon.getDamageMin() + " - " + this.weapon.getDamageMax();
    }
}
