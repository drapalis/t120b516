package edu.ktu.t120b516.rpg;

public class PlayerAppearanceMemento {

    private String gender;
    private String hair;
    private String body;
    private String shirt;
    private String pants;
    private String shoes;

    PlayerAppearanceMemento(String gender, String hair, String body, String shirt, String pants, String shoes) {
        this.gender = gender;
        this.hair = hair;
        this.body = body;
        this.shirt = shirt;
        this.pants = pants;
        this.shoes = shoes;
    }

    public String getGender() {
        return gender;
    }

    public String getHair() {
        return hair;
    }

    public String getBody() {
        return body;
    }

    public String getShirt() {
        return shirt;
    }

    public String getPants() {
        return pants;
    }

    public String getShoes() {
        return shoes;
    }
}
