package edu.ktu.t120b516.rpg;

import java.util.Random;

public class Rabbit {
    private Vector2 position;
    private Random r;

    Rabbit () {
        r = new Random();
        position = new Vector2(r.nextDouble(), r.nextDouble());
    }

    void hop() {
        position.setX(position.getX() + r.nextDouble()-.5);
        position.setY(position.getY() + r.nextDouble()-.5);
    }

    Vector2 getPosition() {
        return position;
    }

}
