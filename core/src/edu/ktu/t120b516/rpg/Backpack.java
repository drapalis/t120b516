package edu.ktu.t120b516.rpg;
import java.util.ArrayList;
import java.util.Iterator;

public class Backpack extends BackpackObject{

	ArrayList backpackObjects = new ArrayList();
	
	String backpackName;
	
	public Backpack(String backpackName) {
		this.backpackName = backpackName;
	}
	
	public String getBackpackName() { return backpackName; }
	
	public void add(BackpackObject backpackObject) {
		backpackObjects.add(backpackObject);
	}
	
	public void remove(BackpackObject backpackObject) {
		backpackObjects.remove(backpackObject);
	}
	
	public BackpackObject getObject(int objectIndex) {
		return (BackpackObject) backpackObjects.get(objectIndex);
	}
	
	public void showContent(int level) {
		
		String levelSeperator = "";
		for(int i = 0; i < level; i++) {
			levelSeperator += "	";
		}
		
		System.out.println(levelSeperator+ backpackName);
		Iterator backpackIterator = backpackObjects.iterator();
		level++;
		
		while(backpackIterator.hasNext()) {
			BackpackObject backpackObject = (BackpackObject) backpackIterator.next();
			backpackObject.showContent(level);
		}
	}

	public IRealItem findInBackpack(String itemName) {
		Iterator backpackIterator = backpackObjects.iterator();

		while(backpackIterator.hasNext()) {
			BackpackObject backpackObject = (BackpackObject) backpackIterator.next();
			IRealItem tempItem = backpackObject.findInBackpack(itemName);
			if(tempItem != null)
				return tempItem;
		}
		return new NullItem();
	}
	
}
