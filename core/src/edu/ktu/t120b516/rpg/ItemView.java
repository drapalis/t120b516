package edu.ktu.t120b516.rpg;

public abstract class ItemView {

    IItem item;

    ItemView(IItem item) {
        this.item = item;
    }

    public void show() {
    }
}
