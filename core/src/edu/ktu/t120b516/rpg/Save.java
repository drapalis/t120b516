package edu.ktu.t120b516.rpg;

import java.io.*;

public class Save {

    private static Save instance = null;
    private PlayerData loadedData;

    private Save() {
        loadData();
    }

    public static Save getInstance() {
        if (instance == null) {
            instance = new Save();
        }
        return instance;
    }

    private void loadData() {
        loadedData = new PlayerData();

        File player = new File("player.txt");

        if(!player.exists()) {
            loadedData.setLevel(1);
            loadedData.setExp(0);
        } else {

            InputStream ins = null;
            Reader r = null;
            BufferedReader br = null;
            try {
                String s;
                ins = new FileInputStream(player);
                r = new InputStreamReader(ins, "UTF-8");
                br = new BufferedReader(r);
                s = br.readLine();
                br.close();
                r.close();
                ins.close();

                String[] data = s.split("\\|");

                loadedData.setLevel(Integer.parseInt(data[0]));
                loadedData.setExp(Integer.parseInt(data[1]));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public PlayerData getLoadedData() {
        return loadedData;
    }

    public void save(PlayerData playerData) {
        if (playerData != null) {

            FileOutputStream outs = null;
            OutputStreamWriter w = null;
            try {
                outs = new FileOutputStream("player.txt");
                w = new OutputStreamWriter(outs, "UTF-8");
                BufferedWriter out = new BufferedWriter(w);
                out.write(playerData.getLevel() + "|" + playerData.getExp());
                out.close();
                w.close();
                outs.close();
            } catch (Exception e){
                e.printStackTrace();
            }
            //updating previously loaded data with new saved data
            this.loadedData = playerData;
        }
    }

}

/*

		PlayerData playerData = Save.getInstance().loadData();
        System.out.println("LVL" + playerData.getLevel());
        System.out.println("EXP" + playerData.getExp());
        playerData.setLevel(playerData.getLevel() + 1);
        playerData.setExp(playerData.getExp() * 2 + 1);
        Save.getInstance().save(playerData);
*/