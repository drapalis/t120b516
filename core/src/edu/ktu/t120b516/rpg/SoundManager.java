package edu.ktu.t120b516.rpg;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class SoundManager {
	private Sound[] sound = new Sound[1];
	
	public void loadSounds() {
		sound[0] = Gdx.audio.newSound(Gdx.files.internal("sounds/sound.ogg"));
		System.out.println("Loading sounds using SoundManager");
	}
	
	public Sound getSound(int id) {
		return sound[id];
	}
}
