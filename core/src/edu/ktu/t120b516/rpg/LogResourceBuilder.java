package edu.ktu.t120b516.rpg;

public class LogResourceBuilder implements IResourceBuilder {

    private ResourceEntity resourceEntity;

    LogResourceBuilder() {
        this.resourceEntity = new ResourceEntity();
    }

    @Override
    public void buildName() {
        this.resourceEntity.setName("Stick");
    }

    @Override
    public void buildImage() {
        this.resourceEntity.setImage(0); //TODO: set actual image from sprite manager
    }

    @Override
    public void buildMinSkillLevel() {
        this.resourceEntity.setMinSkillLevel(1);
    }

    @Override
    public void buildSkillRequired() {
        this.resourceEntity.setSkillRequired(0); //TODO: set actual skill id when skill enumerator/manager is made
    }

    @Override
    public void buildAvailableItemDrops() {

        Resource stick = new Resource();
        stick.setName("Stick");
        stick.setImage(0); // TODO: set actual image for pebble
        stick.setDescription("Stick, should use for picking teeth only");
        Resource leaves = new Resource();
        leaves.setName("Leaves");
        leaves.setImage(0); // TODO: set actual image for pebble
        leaves.setDescription("Leaves, well... use it for lighting fire?");

        //TODO: should make item / resource manager with all available resources pre-defined

        Resource drops[] = new Resource[] {stick, leaves};
        this.resourceEntity.setAvailableDrops(drops);
    }

    @Override
    public ResourceEntity getResource() {
        return this.resourceEntity;
    }
}
